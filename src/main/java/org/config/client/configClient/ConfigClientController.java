package org.config.client.configClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by issam on 3/14/18.
 */
@RestController
public class ConfigClientController {

    @Value("${foo}")
    private  String foo;

    @RequestMapping("/value")
    public String greeting() {
        return "message :"+this.foo;
    }


}
